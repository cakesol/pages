<?php

namespace Cakesol\Pages\Controller;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     */
    public function display()
    {
        
    }
}
